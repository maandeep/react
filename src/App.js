import React from 'react';
import './App.css';
import Header from './components/header'
import TemporaryDrawer from './components/navbar'

function App() {
  let names = ['a', 'b']
  return (
    <div className="App">
      <Header />
      <TemporaryDrawer names={names} />
    </div>
  );
}

export default App;
